<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class IngredientToMeasurementType extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'ingredient_to_measurement_types';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ingredients_id',
        'measurement_types_id',
    ];

    public function measurementTypes()
    {
        return $this->belongsToMany(
            Ingredient::class,
            'ingredient_to_measurement_types',
            'measurement_types_id',
            'ingredients_id'
        );
    }
}
