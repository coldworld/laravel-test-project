<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ingredient extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'ingredients';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'number_of_calories',
        'number_of_proteins',
        'number_of_carbs',
        'number_of_fat',
        'preparation_text',
        'weight_in_grams',
    ];

    public function measurementTypes()
    {
        return $this->belongsToMany(
            MeasurementType::class,
            'ingredient_to_measurement_types',
            'ingredients_id',
            'measurement_types_id'
        );
    }

    public function ingredientTypes()
    {
        return $this->belongsToMany(
            IngredientType::class,
            'ingredient_to_types',
            'ingredients_id',
            'ingredient_types_id'
        );
    }

    /**
     * Check if the ingredient is suitable according the types ids
     *
     * @param array $ingredientTypesIds
     *
     * @return bool
     */
    public function isSuitable($ingredientTypesIds = [])
    {
        return !IngredientToType::where('ingredients_id', '=', $this->id)
            ->whereIn('ingredient_types_id', $ingredientTypesIds)
            ->exists()
        ;
    }

    /**
     * Get the calories for according the amount and measurement unit id
     *
     * @param int $amount
     * @param int $measurementTypeId
     *
     * @return int
     */
    public function getCalories($amount, $measurementTypeId)
    {
        $measurementType = MeasurementType::join(
                'ingredient_to_measurement_types',
                'ingredient_to_measurement_types.measurement_types_id',
                '=',
                'measurement_types.id'
            )
            ->where('ingredient_to_measurement_types.measurement_types_id', '=', $measurementTypeId)
            ->where('ingredient_to_measurement_types.ingredients_id', '=', $this->id)
            ->first()
        ;

        return $measurementType
            ? ($measurementType->weight * $this->number_of_calories / $this->weight_in_grams) * $amount
            : 0
        ;
    }
}
