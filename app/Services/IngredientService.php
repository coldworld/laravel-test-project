<?php

namespace App\Services;

use App\Base\BaseModelService;
use App\Models\Ingredient;
use App\Models\IngredientToMeasurementType;
use App\Models\IngredientToType;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use League\Flysystem\Exception;

class IngredientService extends BaseModelService
{
    /**
     * Create ingredient
     *
     * @param array $params
     *
     * @return Ingredient
     * @throws Exception
     */
    public function create($params = [])
    {
        try {
            DB::beginTransaction();

            $ingredientModel = new Ingredient();
            $ingredientModel->setRawAttributes(
                $this->filterInputData(
                    $params,
                    $ingredientModel->getFillable()
                )
            );

            $ingredientModel->save();

            if (!empty($params['ingredientTypes'])) {
                $this->addTypesToIngredient(
                    $ingredientModel,
                    IngredientToType::class,
                    'ingredient_types_id',
                    $params['ingredientTypes']
                );
            }

            if (!empty($params['measurementTypes'])) {
                $this->addTypesToIngredient(
                    $ingredientModel,
                    IngredientToMeasurementType::class,
                    'measurement_types_id',
                    $params['measurementTypes']
                );
            }

            DB::commit();

            return $ingredientModel;
        } catch (\PDOException $e) {
            DB::rollBack();

            throw new Exception($e->getMessage());
        }
    }

    /**
     * Add types to ingredient
     *
     * @param Ingredient $ingredient
     * @param array $typesIds
     *
     * @return bool
     */
    public function addTypesToIngredient(Ingredient $ingredient, $typeModel, $relativeAttributeName, $typesIds = [])
    {
        $insertArray = [];

        foreach ($typesIds as $typeId) {
            $insertArray[] = [
                'ingredients_id' => $ingredient->id,
                $relativeAttributeName => $typeId,
            ];
        }

        return $insertArray
            ? $typeModel::insert($insertArray)
            : false
        ;
    }

    /**
     * Filter suitable ingredients
     *
     * @param array $ingredientsArray
     * @param array $ingredientTypesIds
     *
     * @return Collection
     */
    public function filterSuitableIngredients(Collection $ingredientsArray, $ingredientTypesIds = [])
    {
        return $ingredientsArray
                ->filter(function ($ingredient) use ($ingredientTypesIds) {
                /**
                 * @var Ingredient $ingredient
                 */
                return $ingredient->isSuitable($ingredientTypesIds);
            })
            ->values()
        ;
    }
}
