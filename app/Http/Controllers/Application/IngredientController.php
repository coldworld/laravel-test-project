<?php

namespace App\Http\Controllers\Application;

use App\Http\Controllers\Controller;
use App\Models\Ingredient;
use App\Services\IngredientService;

class IngredientController extends Controller
{
    /**
     * List all ingredients
     *
     * @return string
     *
     */
    public function listIngredientsAction(IngredientService $ingredientService)
    {
        return response()->json(
           $ingredientService->filterSuitableIngredients(
               Ingredient::all(),
               [1, 2]
           )
        );
    }

    /**
     * Create new ingredient
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function createIngredientAction(IngredientService $ingredientService)
    {
        return response()->json(
            $ingredientService->create(
                [
                    'name' => 'Onion',
                    'number_of_calories' => 11,
                    'number_of_proteins' => 22,
                    'number_of_carbs' => 33,
                    'number_of_fat' => 55,
                    'preparation_text' => 66,
                    'weight_in_grams' => 100,
                    'ingredientTypes' => [1, 2],
                    'measurementTypes' => [1, 2],
                ]
            )
        );
    }
}
