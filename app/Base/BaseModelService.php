<?php

namespace App\Base;

class BaseModelService
{
    /**
     * Filter input data
     *
     * @param array $inputData
     * @param array $allowedAttributes
     *
     * @return array
     */
    protected function filterInputData($inputData = [], $allowedAttributes = [])
    {
        $filteredInput = [];

        foreach ($inputData as $key => $value) {
            if (!in_array($key, $allowedAttributes)) {
                continue;
            }

            $filteredInput[$key] = $value;
        }

        return $filteredInput;
    }
}
