<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMeasurementTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('measurement_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 45);
            $table->integer('weight')->nullable();
        });

        DB::table('measurement_types')->insert(
            [
                [
                    'name' => '1 slice thin',
                    'weight' => 9,
                ],
                [
                    'name' => '1 cup, sliced',
                    'weight' => 115,
                ],
                [
                    'name' => '1 cup, chopped',
                    'weight' => 160,
                ]
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('measurement_types');
    }
}
