<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIngredientToTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ingredient_to_types', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ingredients_id')->unsigned();
            $table->integer('ingredient_types_id')->unsigned();

            $table
                ->foreign('ingredients_id')
                ->references('id')
                ->on('ingredients')
                ->onDelete('cascade')
            ;
            $table
                ->foreign('ingredient_types_id')
                ->references('id')
                ->on('ingredient_types')
                ->onDelete('cascade')
            ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ingredient_to_types');
    }
}
