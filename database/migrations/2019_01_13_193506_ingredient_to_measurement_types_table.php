<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class IngredientToMeasurementTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ingredient_to_measurement_types', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ingredients_id')->unsigned();
            $table->integer('measurement_types_id')->unsigned();

            $table
                ->foreign('ingredients_id')
                ->references('id')
                ->on('ingredients')
                ->onDelete('cascade')
            ;
            $table
                ->foreign('measurement_types_id')
                ->references('id')
                ->on('measurement_types')
                ->onDelete('cascade')
            ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ingredient_to_measurement_types');
    }
}
