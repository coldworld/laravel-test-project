<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIngredientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ingredients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('number_of_calories')->nullable()->default(100);
            $table->integer('number_of_proteins')->nullable()->default(100);
            $table->integer('number_of_carbs')->nullable()->default(100);
            $table->integer('number_of_fat')->nullable()->default(100);
            $table->integer('weight_in_grams')->default(100);
            $table->text('preparation_text')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ingredients');
    }
}
