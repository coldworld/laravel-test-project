<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIngredientTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ingredient_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
        });

        DB::table('ingredient_types')->insert(
            [
                [
                    'name' => 'Has meet',
                ],
                [
                    'name' => 'Has gluten',
                ],
                [
                    'name' => 'Milk product',
                ]
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ingredient_types');
    }
}
